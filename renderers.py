from rest_framework.renderers import JSONRenderer


class DOWNLOADFileRenderer(JSONRenderer):
    format = 'download'
