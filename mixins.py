import json
from io import BytesIO
from collections import OrderedDict
from django.http import FileResponse
from django.utils import timezone
from rest_framework import serializers
from rest_framework.exceptions import APIException
from rest_framework.response import Response

from .renderers import DOWNLOADFileRenderer


class ImportMixin():

    def check_file_struct(self, data):
        for exporter in self.exporter_classes:
            if exporter.key_name not in data:
                raise APIException('Bad file error: key_name {} is missing.'.format(exporter.key_name))

    def get_file_content(self, request):
        content = request.FILES.get('file').read()
        return json.loads(content.decode())

    def should_import(self, request):
        return True if request.FILES.get('file') is not None else False

    def get_serializer_class(self):
        if self.action in ['create', 'update', ]:
            class ImportSerializer(serializers.Serializer):
                file = serializers.FileField(required=False)

            return ImportSerializer
        
        return super().get_serializer_class()

    def import_from_file(self, request):
        data = self.get_file_content(request)
        self.check_file_struct(data)
        success = []
        errors = []
        key_name = None

        for exporter in self.exporter_classes:
            if exporter.handle_results:
                key_name = exporter.key_name
                break

        if key_name is None:
            raise APIException("key_name is missing, make sur you have implemented an exporter for results.")

        if data.get(key_name) is None:
            raise APIException("key_name is not matching, key_name accepted for results is {}".format(key_name))

        for item in data[key_name]:
            update = False
            queryset = self.queryset.filter(id=item['id'])
            if queryset.exists():
                update = True
                serializer = self.serializer_class(queryset.first(), data=item)
            else:
                serializer = self.serializer_class(data=item)
            if not serializer.is_valid(raise_exception=False):
                errors.append(serializer.errors)
            else:
                try:
                    serializer.save()
                    if update:
                        message = "{} with id {} has been updated".format(self.queryset.model._meta.model_name, item['id'])
                    else:
                        message = "{} with id {} has been created".format(self.queryset.model._meta.model_name, item['id'])
                    success.append(message)
                except Exception as e:
                    errors.append(e)
        return {"success": success, "errors": errors}

    def create(self, request, *args, **kwargs):
        should_import = self.should_import(request)
        if should_import:
            data = self.import_from_file(request)
            return Response(data)
        raise APIException('file is missing')


class ExportMixin():

    def should_download(self):
        
        if hasattr(self.request, 'accepted_renderer') and isinstance(self.request.accepted_renderer, DOWNLOADFileRenderer):
            return True

        return False

    def prepare_export_data(self, data):
        if isinstance(data, dict):
            data = [data]
        obj = OrderedDict()
        has_results_exporter = False
        for exporter in self.exporter_classes:
            if exporter.handle_results:
                has_results_exporter = True
            obj = exporter().render(self, data, obj)
        if not has_results_exporter:
            raise Exception("exporter with handle_results key is missing")
        return obj

    def dispatch(self, request, *args, **kwargs):
        response = super().dispatch(request, *args, **kwargs)
        if self.should_download():
            data = json.dumps(response.data)
            response = FileResponse(BytesIO(data.encode()), content_type='application/json')
            now = timezone.now().strftime('%D').replace('/', '-')
            name = self.queryset.model._meta.model_name
            filename = "{}.export.{}.json".format(name, now)
            response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
        return response

    def list(self, request, format=None):
        # no pagination
        queryset = self.filter_queryset(self.queryset)
        serializer = self.get_serializer_class()(queryset, many=True)
        return Response(self.prepare_export_data(serializer.data))

    def retrieve(self, request, pk=None, format=None):
        response = super().retrieve(request, pk=pk, format=None)
        return Response(self.prepare_export_data(response.data))
