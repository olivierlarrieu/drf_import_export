from rest_framework.renderers import BrowsableAPIRenderer, JSONRenderer
from rest_framework.settings import api_settings
from rest_framework.viewsets import ModelViewSet

from .mixins import ExportMixin, ImportMixin
from .renderers import DOWNLOADFileRenderer

# TODO: implement importer_classes
# importer_classes will allow to apply actions after results have been treated.
# ex: import is done and item have been saved/updated, we need to do some other actions,
# write in a table than an import has been done, create model extra fields, etc ...


class ImportModelViewset(ImportMixin, ModelViewSet):
    """
    Allow to import data.
    """
    importer_classes = []


class ExportModelViewset(ExportMixin, ModelViewSet):
    """
    Allow to export data.
    """
    renderer_classes = [*api_settings.DEFAULT_RENDERER_CLASSES, DOWNLOADFileRenderer]
    exporter_classes = []

class ImportExportModelViewset(ImportMixin, ExportMixin, ModelViewSet):
    """
    Allow to export and import data.
    """
    renderer_classes = [*api_settings.DEFAULT_RENDERER_CLASSES, DOWNLOADFileRenderer]
    exporter_classes = []
    importer_classes = []
