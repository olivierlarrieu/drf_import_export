


class BaseExporter():
    """
    allow to chain the export result object formatting
    and do specific operation like request a microservice for a given key
    """
    handle_results = False
    key_name = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.key_name is None:
            raise Exception('key_name property is missing')

    def render(self, viewset, data, obj={}):
        raise NotImplementedError("Exporter should implement render method.")


class ModelExporter(BaseExporter):
    key_name = "model"
    def render(self, viewset, data, obj={}):
        obj[self.key_name] = viewset.queryset.model._meta.model_name
        return obj


class AppExporter(BaseExporter):
    key_name = "app"
    def render(self, viewset, data, obj={}):
        obj[self.key_name] = viewset.queryset.model._meta.app_label
        return obj


class CountExporter(BaseExporter):
    key_name = "count"
    def render(self, viewset, data, obj={}):
        obj[self.key_name] = len(data)
        return obj


class ContentExporter(BaseExporter):
    handle_results = True
    key_name = "results"
    def render(self, viewset, data, obj={}):
        obj[self.key_name] = data
        return obj


class ExtraFieldsExporter(BaseExporter):
    key_name = "extrafields"
    def render(self, viewset, data, obj={}):
        obj[self.key_name] = []
        return obj
